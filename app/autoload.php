<?php
use Illuminate\Database\Capsule\Manager as Capsule;

// Load composer packages and stuff
require_once('vendor/autoload.php');

// Load .env file
$dotenv = Dotenv\Dotenv::createImmutable(__DIR__);
$envvars = $dotenv->safeLoad();
foreach($envvars as $key => $value) {
    putenv("{$key}={$value}");
}

// Boot Eloquent
$capsule = new Capsule;
$capsule->addConnection([
    'driver' => $envvars['DB_DRIVER'],
    'host' => $envvars[ 'DB_HOST' ],
    'database' => $envvars['DB_DBNAME'],
    'username' => $envvars['DB_USER'],
    'password' => $envvars['DB_PASS'],
    'charset' => 'utf8',
    'collation' => 'utf8_unicode_ci',
    'prefix' => '',
]);

// Make this Capsule instance available globally via static methods... (optional)
$capsule->setAsGlobal();

// Setup the Eloquent ORM... (optional; unless you've used setEventDispatcher())
$capsule->bootEloquent();



// Do autoload stuff here...
