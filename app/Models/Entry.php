<?php 
namespace Models;

use Illuminate\Database\Eloquent\Model;

class Entry extends Model {
    protected $fillable = [
        'state',
        'brightness'
    ];
}
