<?php
namespace Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Light Extends Model {
    public $timestamps = false;
    protected $fillable = [
        'name',
        'unique_id'
    ];

    public function entries(): HasMany {
        return $this->hasMany(Entry::class);
    }

}
