<?php

use Models\Light;

require_once('autoload.php');

$hueIp = getenv('HUE_IP');
$hueUser = getenv('HUE_USER');

$url = "https://{$hueIp}/api/{$hueUser}/lights";

// Used to ignore failing SSL
$arrContextOptions=array(
    "ssl"=>array(
        "verify_peer"=>false,
        "verify_peer_name"=>false,
    ),
);  

$response = file_get_contents("{$url}", false, stream_context_create($arrContextOptions));

$data = json_decode($response);


foreach($data as $lightData) {
    $light = Light::firstOrCreate([
        'unique_id' => $lightData->uniqueid 
    ]);

    $light->name = $lightData->name;
    $light->save();

    $light->entries()->create([
        'brightness' => $lightData->state->bri ?? 0,
        'state' => $lightData->state->on
    ]);
}
